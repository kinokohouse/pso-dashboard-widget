# PSO Tool #
A macOS Dashboard widget to support your Phantasy Star Online Ver. 1 & 2
(and also Blue Burst for a bit) life. Source available under an MIT license.
Made in DashCode. Ready to use binary available from downloads section.
Tested on 10.6.8 and above, should work on systems as early as 10.4.3.
An explanation of the widget follows below.

---

### Section ID Calculator ###
Enter your preferred name and see what Section ID you'll belong to. Check the
Blue Burst checkbox and select your class to calculate your section ID for
PSO Blue Burst.

### Online Time ###
See what the current time in Swatch Beats is to make organising games that
stretch over multiple continents that bit easier.

### Server Status ###
Keep an eye on the status of Sylverant.net, the most popular PSO community
on the net, and its ships. The 'g' means games, 'p' means players, a red dot
before the ship name means the ship is offline. It's all pretty self-explanatory.


---

Building
--------
Editing and deploying can be done from either version of DashCode, or you can
edit the files within the widget bundle with your favourite text/image editors. 

Version info
------------

Current version: 1.8. More specific changes are noted in the source code.
