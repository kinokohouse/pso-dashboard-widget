// PSO Tool 1.8
// Build # (I don't know anymore...)
// The whole slapped together by R-422, but it's based on lots of 
// previous work by others; see the other comments in the source.
//
// Changes since previous version (1.7):
// - Updated to read server status in JSON instead of raw HTML
//   (...which was terrible practice btw, I apologize)
// - This somehow fixes Altimira erroneously showing as offline...
// - ...and also fixes showing Vega with the stats for Altimira
//
// Changes since previous version (1.6):
// - Updated URLs
// - Fixed longstanding cosmetic bug in blackOut() function
//
// Changes since previous version (1.5):
// - Added support for Palmacosta ship


// These variables are ours...

var id = ["VIRIDIA","GREENNILL","SKYLY","BLUEFULL","PURPLENUM","PINKAL","REDRIA","ORAN","YELLOWBOZE","WHITILL"];
var pic= ["viridia.png","greennill.png","skyly.png","bluefull.png","purplenum.png","pinkal.png","redria.png","oran.png","yellowboze.png","whitill.png"];
var sites = ["http://www.ragol.co.uk/","http://www.pso-world.com/","http://www.sega.co.jp/","https://web.archive.org/web/20160526201645/http://dirkeinecke.de/","http://www.apple.com/downloads/dashboard/calculate_convert/swatchbeats.html","http://www.swatch.com/zz_en/internettime/","http://sylverant.net/","https://web.archive.org/web/20110127121744/http://www.freewebs.com/pso-p/sectidvalue.html"]

serverURL = "http://wsgi.sylverant.net/status.py?format=json";

var timerInterval = null;
var statusTimer = null;
var nameTimer = null;
var statusJiffies = 120000;
var nameChangeJiffies = 5000;
var debug = false;
var debugFunc = false;
var debugTarget = false;
var magicKey = 93; // right apple
var debugColor = "navajowhite";
var pLayer = " hunter";
var gAme = " team";

document.addEventListener("keydown",checkForDown,true);
document.addEventListener("keyup",checkForUp,true);

// All stuff below up to 'startTimer()' is Apple's. There's an ugly EULA somewhere in the innards
// of the widget that applies to it.

function load()
{
    dashcode.setupParts();
    getPrefs();
    show();
}

function remove()
{
	setPrefs();
    if (timerInterval != null)
	{
		clearInterval(timerInterval);
		timerInterval = null;
	}
	if (statusTimer != null)
	{
		clearInterval(statusTimer);
		statusTimer = null;
	}
	if (nameTimer != null)
	{
		clearTimeout(nameTimer);
		nameTimer = null;
	}
}

function hide()
{
	setPrefs();
	if (timerInterval != null)
	{
		clearInterval(timerInterval);
		timerInterval = null;
	}
	if (statusTimer != null)
	{
		clearInterval(statusTimer);
		statusTimer = null;
	}
	if (nameTimer != null)
	{
		clearTimeout(nameTimer);
		nameTimer = null;
	}
    if (debug) {
        alert("hide()");
    }
}

function show()
{
	if (timerInterval == null)
	{
		startTimer();
    }
    if (statusTimer == null)
    {
        startStatus();
    }
    calc();
    getServerStatus(serverURL);
    if (debug) {
        alert("show()");
    }
}

function sync()
{
}

function showBack(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
    handleBackStuff();
    if (debug) {
        alert("showBack()");
    }
}

function showFront(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);

    }
    calc();
    if (statusJiffies == 0)
    {
        getServerStatus(serverURL);
    }
    if (debug) {
        alert("showFront()");
    }
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

// Full Disclosure:
// Functions startTimer() and getBeat() were taken from Dirk Einecke's too cool swatch.beats widget -  
// save a few bug fixes and readability tweaks.

function startTimer()
{
	if (timerInterval != null)
	{
		clearInterval(timerInterval);
		timerInterval = null;
	}

	timerInterval = setInterval("updateTime();", 1000);
    if (debug) {
        alert("startTimer()");
    }
}

function startStatus()
{
	if (statusTimer != null)
	{
		clearInterval(statusTimer);
		statusTimer = null;
	}
    if ( statusJiffies > 0)
    {
        statusTimer = setInterval("getServerStatus(serverURL);", statusJiffies);
        if (debug)
        {
            alert("Status timer set to " + statusJiffies + " jiffies");
        }
    }
    else
    {
        if (debug)
        {
            alert("Status timer halted - will refresh at every show()");
        }
    }
    if (debug) {
        alert("startStatus()");
    }
}

function getBeat()
{
	var now = new Date();
	var off = (now.getTimezoneOffset() + 60) * 60;
	var theSeconds = (now.getHours() * 3600) + (now.getMinutes() * 60) + now.getSeconds() + off;
	var beat = Math.floor(theSeconds / 86.4);
	if (beat > 1000) {
        beat -= 1000;
        }
	if (beat < 0) {
        beat += 1000;
        }
	return beat;
}

function getTime()
{
	var theNow = new Date();
    var theTime = "";
    var theHours = theNow.getHours();
    var theMinutes = theNow.getMinutes();
    if (theHours < 10) {
        theHours = '0' + theHours;
        }
    if (theMinutes < 10) {
        theMinutes = '0' + theMinutes;
        }
    theTime = theHours + ":" + theMinutes;
    return theTime;
}

function getCal()
{
	var theNow = new Date();
    var theDate = "";
    var theYear = theNow.getYear();
    theYear -= 100;
    var theMonth = theNow.getMonth();
    theMonth ++;
    if (theMonth < 10) {
        theMonth = '0' + theMonth;
        }
    var theDay = theNow.getDate();
    if (theDay < 10) {
        theDay = '0' + theDay;
        }
    theDate = "d" + theDay + "." + theMonth + "." + theYear;
    return theDate;
}

function updateTime()
{
	var beat = String(getBeat());
    if (beat.length < 2) {
        beat = '0' + beat;
        }  
    if (beat.length < 3) {
        beat = '0' + beat;
        }  
	document.getElementById('beats-text').innerText = '@' + beat;
    document.getElementById('time-text').innerText = getTime();
    document.getElementById('date-text').innerText = getCal();
}

// Full Disclosure:
// Basic algorithm taken from Ragol.co.uk, with adaptations from PSO-World.com / Xfox Prowler
// (basic BB compatibility) and PSO-p (BB Unicode additions). 

function calc()
{
    var textAreaToChange = document.getElementById("section-id");
    var name = document.getElementById("name-input").value;
    var newTextAreaText = "";
    var pixToChange = document.getElementById("section-image");
    var pixToChangeToo = document.getElementById("section-image-2");
    var newPix = "empty.png";
    var x = 0;
    var valCorr = 0;
    var lastValCorr = 0;
    var invalidName = false;
    if (check.checked == true)
    {
        var v = document.getElementById("menu");
        x = parseInt(v.object.getValue(),10);
    }
    for(i=0,k=0;i<name.length;i++)
    {
        j=name.charCodeAt(i);
        if (check.checked == false)
        {
            if (j<32 || j>126)
            {
                j = 0;
                invalidName = true;
            }
        }
        k+=j;
        if (check.checked == true)
        {
            valCorr = (j <= 0x00FF || (j >= 0xFF61 && j <= 0xFF91))? 5 : 3;
            if (valCorr != lastValCorr) k+=valCorr;
            lastValCorr = valCorr;
        }
    }
    k+=x;
    if(invalidName == true || name.length == 0)
    {
    k = 0;
        newTextAreaText = "???";
        textAreaToChange.innerText = newTextAreaText;
        newPix = "empty.png";
        pixToChange.firstElementChild.src = ("Images/" + newPix);
        pixToChangeToo.firstElementChild.src = ("Images/" + newPix);
    }
    else
    {
        newTextAreaText = id[k%10];
        newPix = pic[k%10];
        textAreaToChange.innerText = newTextAreaText;
        pixToChange.firstElementChild.src = ("Images/" + newPix);
        pixToChangeToo.firstElementChild.src = ("Images/" + newPix);
    }
    if (debug) {
        alert("calc()");
    }
}

function checkItOut()
{
    if (check.checked == true)
    {
        menu.style.opacity = 1.0;
        menu.object.setEnabled(true);
        truncateAndSetShort();
        calc();
    }
    else
    {
        menu.style.opacity = 0.3;
        menu.object.setEnabled(false);
        setLong();
        calc();
    }
    widget.setPreferenceForKey(check.checked,"checkBoxState");
    if (debug)
    {
        alert("checkItOut()");
    }
}


function labelClick(event)
{
    if (check.checked == true)
    {
        check.checked = false;
        checkItOut();
    }
    else
    {
        check.checked = true;
        checkItOut();
    }
}

function truncateAndSetShort()
{
    var x = document.getElementById("name-input");
    var n = x.value;
    var l = n.length;
    if (l > 10)
    {
        var m = n.substr(0,10);
        x.value = m;
    }
    x.maxLength = 10;
}

function setLong()
{
    var x = document.getElementById("name-input");
    x.maxLength = 12;
}

function getServerStatus(pass)
{
    try
    {
        document.getElementById("loading-indicator-l").style.opacity = 0.3;
        document.getElementById("loading-indicator-r").style.opacity = 0.3;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4)
            {
                if (xmlhttp.status == 200)
                {
                    m = xmlhttp.responseText;
                    parseServerStatus(m);
                    document.getElementById("loading-indicator-l").style.opacity = 0;
                    document.getElementById("loading-indicator-r").style.opacity = 0;
                    if (debug)
                    {
                        alert("Loaded data from Sylverant.net A-OK.");
                    }
                }
                else
                {
                    blackOut();
                    alert("Something went wrong when loading info from sylverant.net. It is most probably a temporary problem; please try again later.")
                }
            }
        }
        xmlhttp.open("GET",pass,true);
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.send();
        xmlhttp.overrideMimeType("text");
    }
    catch (e)
    {
        blackOut();
        alert("XMLHttpRequest to sylverant.net threw an exception.");
        alert(e.name)
        alert(e.message)        
    }
}

function parseServerStatus(message)
{
    var m = JSON.parse(message);
    var p = 0;
    var g = 0;
    var t = 0;
    var q = 0;
    
    // patch server status
    
    if (m.patch.status !== "online") {
        document.getElementById("patch-players").innerHTML = "-";
        document.getElementById("patch-label").style.opacity = 0.3;
        document.getElementById("patch-players").style.opacity = 0.3;
        document.getElementById("patch-p").style.opacity = 0.3;
        document.getElementById("patch-indicator-red").style.opacity = 1.0;
        document.getElementById("patch-indicator-green").style.opacity = 0.0;
        if (debug) {alert("Patch server appears to be offline.")}
    } else {
    	var tp = "";
        if (m.patch.connection !== null) {
        	tp = "" + m.patch.connections;
        } else {
        	tp = "0";
        }
        document.getElementById("patch-players").innerHTML = tp;
        document.getElementById("patch-label").style.opacity = 1.0;
        document.getElementById("patch-players").style.opacity = 1.0;
        document.getElementById("patch-p").style.opacity = 1.0;
        document.getElementById("patch-indicator-green").style.opacity = 1.0;
        document.getElementById("patch-indicator-red").style.opacity = 0.0;
        if (debug) {alert("Patch server: " + tp +" player(s) connected.")}
    }

    // login server status
    
    if (m.login.status !== "online") {
        document.getElementById("login-players").innerHTML = "-";
        document.getElementById("login-label").style.opacity = 0.3;
        document.getElementById("login-players").style.opacity = 0.3;
        document.getElementById("login-p").style.opacity = 0.3;
        document.getElementById("login-indicator-red").style.opacity = 1.0;
        document.getElementById("login-indicator-green").style.opacity = 0.0;
        if (debug) {alert("Login server appears to be offline.")}
    } else {
    	var tp = "";
		if (m.login.connections !== null) {
        	tp = "" + m.login.connections;
        } else {
        	tp = "0";
        }
        document.getElementById("login-players").innerHTML = tp;
        document.getElementById("login-label").style.opacity = 1.0;
        document.getElementById("login-players").style.opacity = 1.0;
        document.getElementById("login-p").style.opacity = 1.0;
        document.getElementById("login-indicator-green").style.opacity = 1.0;
        document.getElementById("login-indicator-red").style.opacity = 0.0;
        if (debug) {alert("Login server: " + tp +" player(s) connected.")}
    }
    document.getElementById("separator").style.opacity = 1.0;
        
    t = m.ships;
    invalidateAllShips();
    for (i = 0; i < t.length; i++) {
    	validateShip(t[i]);
    }
}

// Validate generic ship

function validateShip(shipObject) {
	var na = shipObject.name;
	var tn = na.toLowerCase();
	var tg = "";
	var tp = "";
	if (shipObject.teams !== null) {
		tg = "" + shipObject.teams;
	} else {
		tg = "0";
	}
	if (shipObject.connections !== null) {
		tp = "" + shipObject.connections;
	} else {
		tp = "0";
	}
	document.getElementById(tn + "-games").innerHTML = tg;
	document.getElementById(tn + "-players").innerHTML = tp;
	document.getElementById(tn + "-label").style.opacity = 1.0;
	document.getElementById(tn + "-games").style.opacity = 1.0;
	document.getElementById(tn + "-players").style.opacity = 1.0;
	document.getElementById(tn + "-g").style.opacity = 1.0;
	document.getElementById(tn + "-p").style.opacity = 1.0;
	document.getElementById(tn + "-indicator-green").style.opacity = 1.0;
	document.getElementById(tn + "-indicator-red").style.opacity = 0.0;
	if (debug) {alert(na + ": " + tp + " player(s), " + tp + " game(s).");}
}

function invalidateAllShips() {
        document.getElementById("iselia-players").innerHTML = "-";
        document.getElementById("iselia-games").innerHTML = "-";
        document.getElementById("iselia-label").style.opacity = 0.3;
        document.getElementById("iselia-players").style.opacity = 0.3;
        document.getElementById("iselia-games").style.opacity = 0.3;
        document.getElementById("iselia-p").style.opacity = 0.3;
        document.getElementById("iselia-g").style.opacity = 0.3;
        document.getElementById("iselia-indicator-red").style.opacity = 1.0;
        document.getElementById("iselia-indicator-green").style.opacity = 0.0;
        document.getElementById("altimira-players").innerHTML = "-";
        document.getElementById("altimira-games").innerHTML = "-";
        document.getElementById("altimira-label").style.opacity = 0.3;
        document.getElementById("altimira-players").style.opacity = 0.3;
        document.getElementById("altimira-games").style.opacity = 0.3;
        document.getElementById("altimira-p").style.opacity = 0.3;
        document.getElementById("altimira-g").style.opacity = 0.3;
        document.getElementById("altimira-indicator-red").style.opacity = 1.0;
        document.getElementById("altimira-indicator-green").style.opacity = 0.0;
        document.getElementById("vega-players").innerHTML = "-";
        document.getElementById("vega-games").innerHTML = "-";
        document.getElementById("vega-label").style.opacity = 0.3;
        document.getElementById("vega-players").style.opacity = 0.3;
        document.getElementById("vega-games").style.opacity = 0.3;
        document.getElementById("vega-p").style.opacity = 0.3;
        document.getElementById("vega-g").style.opacity = 0.3;
        document.getElementById("vega-indicator-red").style.opacity = 1.0;
        document.getElementById("vega-indicator-green").style.opacity = 0.0;
        document.getElementById("palmacosta-players").innerHTML = "-";
        document.getElementById("palmacosta-games").innerHTML = "-";
        document.getElementById("palmacosta-label").style.opacity = 0.3;
        document.getElementById("palmacosta-players").style.opacity = 0.3;
        document.getElementById("palmacosta-games").style.opacity = 0.3;
        document.getElementById("palmacosta-p").style.opacity = 0.3;
        document.getElementById("palmacosta-g").style.opacity = 0.3;
        document.getElementById("palmacosta-indicator-red").style.opacity = 1.0;
        document.getElementById("palmacosta-indicator-green").style.opacity = 0.0;
        if (debug) {alert("All ship information reset.")};
}

function handleBackStuff()
{
    document.getElementById("explanation-text-1").innerHTML = "Based on the section ID calculators once found at <a onclick='openSite(0);'>ragol.co.uk</a>, <a onclick='openSite(1);'>PSO World</a> and <a onclick='openSite(7);'>PSO-p</a>. Go visit there, it's ace."
    document.getElementById("explanation-text-2").innerHTML = "Based on <a onclick='openSite(3);'>Dirk Einecke</a>'s too cool <a onclick='openSite(5);'>swatch.beats</a> <a onclick='openSite(4);'>Dashboard widget</a>."
    document.getElementById("explanation-text-3").innerHTML = "Not affiliated with or endorsed by <a onclick='openSite(2);'>Sega</a> in any way."
    document.getElementById("explanation-text-4").innerHTML = "Keep an eye on <a onclick='openSite(6);'>Sylverant</a>,<br />The  Happy PSO Server.™"
}

function openSite(s)
{
    widget.openURL(sites[s]);
    if (debug)
    {
        alert ("openSite()");
    }
}

function backPrefsChanged(event)
{
    var p = document.getElementById("refreshVal").object.getSelectedIndex();
    var v = document.getElementById("refreshVal").object.getValue();
    widget.setPreferenceForKey(p,"refreshValIndex");
    statusJiffies = v;
    if (debug)
    {
        alert ("backPrefsChanged()");
    }
    startStatus();
}

function setPrefs()
{
    var m = check.checked;
    widget.setPreferenceForKey(m,"checkBoxState");
    var n = document.getElementById("menu").object.getSelectedIndex();
    widget.setPreferenceForKey(n,"classIndex");
    var o = document.getElementById("name-input").value;
    widget.setPreferenceForKey(o,"currentName");    
    var p = document.getElementById("refreshVal").object.getSelectedIndex();
    widget.setPreferenceForKey(p,"refreshValIndex");
    widget.setPreferenceForKey(debug,"debugState");
    if (debug)
    {
        alert ("setPrefs()");
    }
}

function getPrefs()
{
    if (window.widget)
    {
        if (widget.preferenceForKey("checkBoxState") === undefined)
        {
            widget.setPreferenceForKey(false,"checkBoxState");
            check.checked = false;
        }
        else
        {
            check.checked = widget.preferenceForKey("checkBoxState");
            checkItOut();
        }
        
        if (widget.preferenceForKey("classIndex") === undefined || isNaN(widget.preferenceForKey("classIndex")))
        {
            widget.setPreferenceForKey(9,"classIndex");
            document.getElementById("menu").object.setSelectedIndex(9);
        }
        else
        {
            document.getElementById("menu").object.setSelectedIndex(widget.preferenceForKey("classIndex"));
        }
        if (widget.preferenceForKey("currentName") === undefined)
        {
            widget.setPreferenceForKey("Alicia Baz","currentName");
            document.getElementById("name-input").value = widget.preferenceForKey("currentName");
        }
        else
        {
            document.getElementById("name-input").value = widget.preferenceForKey("currentName");
        }
        if (widget.preferenceForKey("refreshValIndex") === undefined || isNaN(widget.preferenceForKey("refreshValIndex")))
        {
            widget.setPreferenceForKey(5,"refreshValIndex");
            document.getElementById("refreshVal").object.setSelectedIndex(5);
            statusJiffies = document.getElementById("refreshVal").object.getValue();
        }
        else
        {
            document.getElementById("refreshVal").object.setSelectedIndex(widget.preferenceForKey("refreshValIndex"));
            statusJiffies = document.getElementById("refreshVal").object.getValue();
        }
        if (widget.preferenceForKey("debugState") === undefined)
        {
            widget.setPreferenceForKey(false,"debugState");
            debug = false;
            document.getElementById("name-input").style.backgroundColor = "white";
        }
        else
        {
            debug = widget.preferenceForKey("debugState");
            if (debug) {
                document.getElementById("name-input").style.backgroundColor = debugColor;
            } else {
                document.getElementById("name-input").style.backgroundColor = "white";
            }
        }
        if (debug)
        {
            alert ("getPrefs()");
        }
    }
}

function menuPrefsChanged(event)
{
    var v = menu.object.getSelectedIndex();
    widget.setPreferenceForKey(v,"classIndex");
    calc();
    if (debug)
    {
        alert ("menuPrefsChanged()");
    }
}

function namePrefsChanged(event)
{
    calc();
    if (nameTimer != null)
    {
        clearTimeout(nameTimer);
        nameTimer = null;
    }
    nameTimer = setTimeout("putNamePrefs();",nameChangeJiffies);
}

function putNamePrefs()
{
    v = document.getElementById("name-input").value;
    widget.setPreferenceForKey(v,"currentName");
    if (debug)
    {
        alert ("New name written to Preferences.");
    }
}

function blackOut()
{
    document.getElementById("loading-indicator-l").style.opacity = 0;
    document.getElementById("loading-indicator-r").style.opacity = 0;
    document.getElementById("patch-indicator-green").style.opacity = 0;
    document.getElementById("login-indicator-green").style.opacity = 0;
    document.getElementById("iselia-indicator-green").style.opacity = 0;
    document.getElementById("vega-indicator-green").style.opacity = 0;
    document.getElementById("altimira-indicator-green").style.opacity = 0;
    document.getElementById("palmacosta-indicator-green").style.opacity = 0;
    document.getElementById("patch-indicator-red").style.opacity = 0.3;
    document.getElementById("login-indicator-red").style.opacity = 0.3;
    document.getElementById("iselia-indicator-red").style.opacity = 0.3;
    document.getElementById("vega-indicator-red").style.opacity = 0.3;
    document.getElementById("altimira-indicator-red").style.opacity = 0.3;
    document.getElementById("palmacosta-indicator-red").style.opacity = 0.3;
    document.getElementById("patch-label").style.opacity = 0.3;
    document.getElementById("login-label").style.opacity = 0.3;
    document.getElementById("separator").style.opacity = 0.3;
    document.getElementById("iselia-label").style.opacity = 0.3;
    document.getElementById("vega-label").style.opacity = 0.3;
    document.getElementById("altimira-label").style.opacity = 0.3;
    document.getElementById("palmacosta-label").style.opacity = 0.3;
    document.getElementById("patch-players").innerHTML = "-";
    document.getElementById("patch-players").style.opacity = 0.3;
    document.getElementById("login-players").innerHTML = "-";
    document.getElementById("login-players").style.opacity = 0.3;
    document.getElementById("iselia-players").innerHTML = "-";
    document.getElementById("iselia-players").style.opacity = 0.3;
    document.getElementById("vega-players").innerHTML = "-";
    document.getElementById("vega-players").style.opacity = 0.3;
    document.getElementById("altimira-players").innerHTML = "-";
    document.getElementById("altimira-players").style.opacity = 0.3;
    document.getElementById("palmacosta-players").innerHTML = "-";
    document.getElementById("palmacosta-players").style.opacity = 0.3;
    document.getElementById("patch-p").style.opacity = 0.3;
    document.getElementById("login-p").style.opacity = 0.3;
    document.getElementById("iselia-p").style.opacity = 0.3;
    document.getElementById("vega-p").style.opacity = 0.3;
    document.getElementById("altimira-p").style.opacity = 0.3;
    document.getElementById("palmacosta-p").style.opacity = 0.3;
    document.getElementById("iselia-games").innerHTML = "-";
    document.getElementById("iselia-games").style.opacity = 0.3;
    document.getElementById("vega-games").innerHTML = "-";
    document.getElementById("vega-games").style.opacity = 0.3;
    document.getElementById("altimira-games").innerHTML = "-";
    document.getElementById("altimira-games").style.opacity = 0.3;
    document.getElementById("palmacosta-games").innerHTML = "-";
    document.getElementById("palmacosta-games").style.opacity = 0.3;
    document.getElementById("iselia-g").style.opacity = 0.3;
    document.getElementById("vega-g").style.opacity = 0.3;
    document.getElementById("altimira-g").style.opacity = 0.3;
    document.getElementById("palmacosta-g").style.opacity = 0.3;
    if (debug)
    {
        alert("blackOut()");
    }
}

function checkForDown(event)
{
    var e = event.keyCode;
    if (debug) {alert("Key pressed: code " + e + ".")}
    if (e == magicKey)
    {
        debugFunc = true;
        if (!debug) {
            document.getElementById("button").object.textElement.innerText = "Debug ON";
            debugTarget = true;
        }
        else {
            document.getElementById("button").object.textElement.innerText = "Debug OFF";
            debugTarget = false;
        }
    }
}

function checkForUp(event)
{
    debugFunc = false;
    document.getElementById("button").object.textElement.innerText = "Spin, Bana!";
}

function whatToDo(event)
{
    if (!debugFunc) {
        showFront(event);
    }
    else
    {
        if (debugTarget) {
            debug = true;
            alert("Debug mode is ON.");            
            document.getElementById("name-input").style.backgroundColor = debugColor;
            showFront(event);
        }
        else {
            debug = false;
            alert("Debug mode is OFF.");
            document.getElementById("name-input").style.backgroundColor = "white";
            showFront(event);
        }
        widget.setPreferenceForKey(debug,"debugState");
        if (debug) {
            alert("Written debug state to Preferences.");
        }
    }
}
